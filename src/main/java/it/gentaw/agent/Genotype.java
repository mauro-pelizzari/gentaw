package it.gentaw.agent;

import java.util.HashMap;
import java.util.Map;

public class Genotype {
    
    private Map<String,Gene> characters;

    public Genotype() {
	this.characters = new HashMap<String, Gene>();
    }

    public Map<String, Gene> getCharacters() {
        return characters;
    }

    public void setCharacters(Map<String, Gene> characters) {
        this.characters = characters;
    }

}
