package it.gentaw.agent;

import it.gentaw.base.Unit;

public class Gene extends Unit {
    
    private Object value;

    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }
    
    public Gene(final String id) {
	super(id);
    }

    public Gene(String id,String name, Object value) {
	super(id);
	setName(name);
	this.value = value;
    }
    
}
