package it.gentaw.agent;

import it.gentaw.base.Unit;

import java.util.List;

public abstract class Agent extends Unit {

    private String type;
    private long health;
    private List<Agent> guests;
    private Genotype genotype;
    private boolean alive = true;

    public Agent(final String id) {
	super(id);
    }
    
    public long getHealth() {
	return health;
    }
    public List<Agent> getGuests() {
	return guests;
    }
    public Genotype getGenotype() {
	return genotype;
    }
    public void setHealth(long health) {
	this.health = health;
    }
    public void incrementHealth(long delta) {
	this.health += delta;
    }
    public void decreaseHealth(long delta) {
	this.health -= delta;
    }
    public void setGuests(List<Agent> guests) {
	this.guests = guests;
    }
    public String getType() {
	return type;
    }
    public void setType(String type) {
	this.type = type;
    }
    public void setGenotype(Genotype genotype) {
	this.genotype = genotype;
    }
    public boolean isAlive() {
	return alive;
    }
    public void setAlive(boolean alive) {
	this.alive = alive;
    }
    public void addGuest(Agent guest) {
	this.guests.add(guest);
    }
    public void removeGuest(Agent guest) {
	this.guests.remove(guest);
    }

    public abstract <T extends Effect> T action(Stimulus stimulus);

    //TODO:inserire qui i metodi per la caratterizzazione del sistema ad agenti
}
