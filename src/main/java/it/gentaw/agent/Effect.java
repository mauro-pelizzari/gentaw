package it.gentaw.agent;

import it.gentaw.base.Unit;

public abstract class Effect extends Unit {
    
    public Effect(String id) {
	super(id);
    }

    public abstract void execute() throws Exception;
    
}
