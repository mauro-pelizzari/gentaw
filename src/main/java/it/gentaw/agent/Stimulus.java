package it.gentaw.agent;

import it.gentaw.base.Unit;

import java.util.HashMap;
import java.util.Map;

public class Stimulus extends Unit {
    
    private Map<String,Object> inputs;

    public Stimulus(final String id) {
	super(id);
	this.inputs = new HashMap<String, Object>();
    }

    public Map<String, Object> getInputs() {
        return inputs;
    }

    public void setInputs(Map<String, Object> inputs) {
        this.inputs = inputs;
    }

}
