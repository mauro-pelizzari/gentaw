package it.gentaw.graph.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * Vertex of the graph
 * @author mauro
 *
 */
public class Vertex extends BaseStructure {

	private String description;
	private Map<String,Edge> edges = new HashMap<String,Edge>();

	public Vertex() {
		super(UUID.randomUUID().toString(),"");
	}
	public Vertex(final String id) {
		super(id,"");
	}
	public Vertex(final String id,final String name) {
		super(id,name);
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Map<String, Edge> getEdges() {
		return edges;
	}
	public void setEdges(Map<String, Edge> edges) {
		this.edges = edges;
	}
	public void addEdge(Edge edge) {
		edges.put(edge.getId(),edge);
	}
	public Edge getEdge(String id) {
		return this.getEdges().get(id);
	}
	public Map<String,Vertex> getLinkedVertex() {
		Map<String,Vertex> r = new HashMap<String,Vertex>();
		for(Entry<String,Edge> e : edges.entrySet()) {
			Edge edge = e.getValue();
			if (this.equals(edge.getVertexA())) {
				r.put(edge.getVertexB().getId(), edge.getVertexB());
			} else {
				r.put(edge.getVertexA().getId(), edge.getVertexA());
			}
		}
		return r;
	}
}
