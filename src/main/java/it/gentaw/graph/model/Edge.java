package it.gentaw.graph.model;


import java.util.List;
import java.util.UUID;

/**
 * Edge object of the graph
 * @author mauro
 *
 */
public class Edge extends BaseStructure {

	private String description;
	private double weight;
	private Vertex vertexA;
	private Vertex vertexB;
	private List<Rule> rules;

	public Edge() {
		super(UUID.randomUUID().toString(),"");
	}
	
	public Edge(final String id,final String name) {
		super(id,name);
	}

	public String getDescription() {
		return description;
	}
	public double getWeight() {
		return weight;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public Vertex getVertexA() {
		return vertexA;
	}
	public Vertex getVertexB() {
		return vertexB;
	}
	public void setVertexA(Vertex vertexA) {
		this.vertexA = vertexA;
	}
	public void setVertexB(Vertex vertexB) {
		this.vertexB = vertexB;
	}

	public final List<Rule> getRules() {
		return rules;
	}

	public final void setRules(List<Rule> rules) {
		this.rules = rules;
	}

}
