package it.gentaw.graph.model;

import java.util.HashMap;
import java.util.Map;

public class Graph extends BaseStructure {

	private Map<String,Vertex> vertices = new HashMap<String, Vertex>();
	private Map<String,Edge> edges = new HashMap<String, Edge>();

	public Graph(final String id) {
		this(id,"");
	}
	public Graph(final String id,final String name) {
		super(id,name);
	}
	public Map<String,Vertex> getVertices() {
		return this.vertices;
	}
	public void addVertex(Vertex v) {
		this.vertices.put(v.getId(),v);
	}
	public Vertex getVertex(String id) {
		return this.vertices.get(id);
	}
	public Map<String, Edge> getEdges() {
		return edges;
	}
	public void addEdge(Edge e) {
		this.edges.put(e.getId(), e);
	}
}
