package it.gentaw.graph.model;

public class BaseStructure {

	private final String id;
	private String name;

	public BaseStructure(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public final String getId() {
		return id;
	}
	public final String getName() {
		return name;
	}
	public final void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (o==null) 
			return false;
		else {
			BaseStructure b = (BaseStructure) o;
			return b.getId().equals(getId());
		}
	}


}
