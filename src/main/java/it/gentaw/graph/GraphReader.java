package it.gentaw.graph;

import it.gentaw.graph.model.Graph;

public interface GraphReader {
	
	public Graph read() throws Exception;
	
}