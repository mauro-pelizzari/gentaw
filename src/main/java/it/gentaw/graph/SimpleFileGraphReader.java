package it.gentaw.graph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;

import it.gentaw.graph.model.Edge;
import it.gentaw.graph.model.Graph;
import it.gentaw.graph.model.Vertex;

public class SimpleFileGraphReader extends GraphFileReader {
	
	private void buildStructure(Graph g,final String chainStructure) {
		if (chainStructure!=null) {
			String[] groupsOfVerteces = chainStructure.split(";");
			for (String group : groupsOfVerteces) {
				String[] appo = group.split(" ");
				Vertex master = new Vertex(appo[0]);
				master.setName(appo[0]);
				if (appo.length>1) {
					String[] slaveNameVerteces = appo[1].split(",");
					for (String nameSlaveVertex : slaveNameVerteces) {
						Vertex slave = new Vertex(nameSlaveVertex);
						slave.setName(nameSlaveVertex);
						Edge e = new Edge();
						e.setVertexA(master);
						e.setVertexB(slave);
						slave.addEdge(e);
						master.addEdge(e);
						g.addEdge(e);
						g.getVertices().put(slave.getId(), slave);
					}
				}
				g.getVertices().put(master.getId(), master);
			}
		}
	}

	public SimpleFileGraphReader(File graphFile) {
		super(graphFile);
	}

	@Override
	public Graph read() throws Exception {
		Reader in = new FileReader(graphFile);
		BufferedReader bf = new BufferedReader(in);
		String id = bf.readLine();
		String name = bf.readLine();
		Graph g = new Graph(id);
		g.setName(name);
		buildStructure(g,bf.readLine());
		bf.close();
		return g;
	}

}
