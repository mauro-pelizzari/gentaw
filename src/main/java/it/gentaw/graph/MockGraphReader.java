package it.gentaw.graph;

import java.util.UUID;

import it.gentaw.graph.model.Graph;

public class MockGraphReader implements GraphReader {

	@Override
	public Graph read() throws Exception {
		return new Graph(UUID.randomUUID().toString());
	}

}
