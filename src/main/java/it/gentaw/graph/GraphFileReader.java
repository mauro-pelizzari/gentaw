package it.gentaw.graph;

import java.io.File;

import it.gentaw.graph.model.Graph;

public abstract class GraphFileReader implements GraphReader {

	protected File graphFile;
	
	public GraphFileReader(File graphFile) {
		this.graphFile = graphFile;
	}

	@Override
	public abstract Graph read() throws Exception;
		

}
