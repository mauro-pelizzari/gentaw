package it.gentaw.base;

import it.gentaw.agent.Agent;

public class Unit {
    
    private final String id;
    private String name;
    
    public Unit(final String id) {
	this.id = id;
    }
    
    public final String getId() {
        return id;
    }
    public final String getName() {
        return name;
    }
    public final void setName(String name) {
        this.name = name;
    }
    
    @Override
    public boolean equals(Object o) {
	if (o==null) 
	    return false;
	else {
	    Agent a = (Agent) o;
	    return a.getId().equals(getId());
	}
    }


}
