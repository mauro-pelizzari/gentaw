package it.gentaw.fact;

public interface Fact {
	public void happen();
}
