package it.gentaw.graph;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;
import it.gentaw.graph.model.Graph;
import it.gentaw.graph.model.Vertex;

public class GraphReaderTest {
	
	private ClassLoader getClassLoader() {
		ClassLoader cld = getClass().getClassLoader();
		return cld;
	}
	
	private File getFileGraph() {
		final File f = new File(getClassLoader().getResource("it/gentaw/graph/graph.txt").getFile());
		return f;
	}
	
	@Test
	public void read() throws Exception {
		GraphReader reader = new MockGraphReader();
		Graph g = reader.read();
		assertNotNull(g);
	}
	
	@Test
	public void readBasicInfoFromFile() throws Exception {
		final File f = getFileGraph();
		GraphReader reader = new SimpleFileGraphReader(f);
		Graph g = reader.read();
		assertEquals(g.getId(), "1");
		assertEquals(g.getName(), "grafico_prova");
	}
	
	@Test
	public void readStructure() throws Exception {
		final File f = getFileGraph();
		GraphReader reader = new SimpleFileGraphReader(f);
		Graph g = reader.read();
		assertTrue(g.getVertices().size()>0);
		Vertex v = g.getVertex("A");
		Vertex vertexC = v.getLinkedVertex().get("C");
		assertNotNull(vertexC);
		assertTrue(vertexC.getLinkedVertex().containsKey("A"));
		assertEquals(g.getEdges().size(),5);
	}

}
